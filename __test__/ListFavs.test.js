const axios = require("axios");
const { generateString } = require("../helpers/random-string.js");

describe("ListFavs tests", () => {
  let ListfavsMock;
  let ListfavsMockNew;
  let token;
  let idUser;
  let idListFav;
  let invalidID;
  let validID;

  beforeAll(async () => {
    ListfavsMock = {
      nameListFav: `lista Favoritos ${generateString()}`,
    };
    ListfavsMockNew = {
      nameListFav: `lista Favoritos ${generateString()}`,
    };
    invalidID = "12323asda";
    validID = "1234567890abcdefghij1234";
    // Login to get token
    const instance = axios.create({
      // Nota: tuve problemas al correr en local 8082, asi que en 5000 me funciono
      baseURL: "http://localhost:5000",
      timeout: 1000,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });

    let data = {
      email: "jorge150896@hotmail.com",
      password: "123456@Jv",
    };

    try {
      const login = await instance.post("/auth/local/login/", data);
      const { token: tokenUser, usuario: idUserMongo } = login.data;
      token = tokenUser;
      idUser = idUserMongo._id;
    } catch (error) {
      console.log(error.response);
    }
    console.log("token", token);
  });

  // ✅✔️✅
  it("Success create a ListFavs ", async () => {
    const result = await axios.post(
      "http://localhost:5000/api/listfavs",
      ListfavsMock,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    idListFav = result.data._id;

    expect(result.data).toBeDefined();
    expect(result.status).toEqual(201);
    expect(result.data.nameListFav).toEqual(
      ListfavsMock.nameListFav.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo creo
    expect(result.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(result.data._id.length).toBe(24);
  });

  // ❌🔴❌
  it("Error create a ListFavs Without Name", async () => {
    try {
      await axios.post(
        "http://localhost:5000/api/listfavs",
        { nameListFav: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ❌🔴❌
  it("Error create a ListFavs With name already registered", async () => {
    try {
      await axios.post("http://localhost:5000/api/listfavs", ListfavsMock, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.msg).toContain(
        `La Lista de Favoritos ${ListfavsMock.nameListFav.toUpperCase()}, ya existe`
      );
    }
  });

  // ✅✔️✅

  it("Success get all ListFavs", async () => {
    const result = await axios.get("http://localhost:5000/api/listfavs");
    expect(result.status).toEqual(200);
    expect(Array.isArray(result.data.listfavs)).toEqual(true);
    expect(result.data.listfavs.length).toBeGreaterThan(0);
  });

  // ✅✔️✅

  it("Success get one ListFav", async () => {
    const listfavsGet = await axios.get(
      `http://localhost:5000/api/listfavs/${idListFav}`
    );

    expect(listfavsGet.status).toEqual(200);
    expect(listfavsGet.data.listfav.nameListFav).toEqual(
      ListfavsMock.nameListFav.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo creo
    expect(listfavsGet.data.listfav.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(listfavsGet.data.listfav._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error get one ListFav with ID invalid", async () => {
    try {
      await axios.get(`http://localhost:5000/api/listfavs/${invalidID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error get one ListFav with ID Valid Non-existent ", async () => {
    try {
      await axios.get(`http://localhost:5000/api/listfavs/${validID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      expect(data.errors[0].value.length).toBe(24);
    }
  });

  // ✅✔️✅
  it("Success Update one ListFav", async () => {
    const listfavUpdate = await axios.put(
      `http://localhost:5000/api/listfavs/${idListFav}`,
      ListfavsMockNew,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    expect(listfavUpdate.status).toEqual(200);
    expect(listfavUpdate.data.nameListFav).toEqual(
      ListfavsMockNew.nameListFav.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo actualizo
    expect(listfavUpdate.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(listfavUpdate.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Update one ListFav Without Name", async () => {
    try {
      await axios.put(
        `http://localhost:5000/api/listfavs/${idListFav}`,
        { nameListFav: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ✅✔️✅

  it("Success Delete one ListFavs", async () => {
    // search id to test
    const listfavDelete = await axios.delete(
      `http://localhost:5000/api/listfavs/${idListFav}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    expect(listfavDelete.status).toEqual(200);
    expect(listfavDelete.data.nameListFav).toEqual(
      ListfavsMockNew.nameListFav.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo elimino
    expect(listfavDelete.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(listfavDelete.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Delete one ListFavs with ID invalid", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/listfavs/${invalidID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error Delete one ListFavs with ID Valid Non-existent ", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/listfavs/${validID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).toBe(24);
    }
  });
});
