const axios = require("axios");
const { generateString } = require("../helpers/random-string.js");

describe("Favoritos tests", () => {
  let FavsMock;
  let FavsMockNew;
  let token;
  let idUser;
  let idFav;
  let invalidID;
  let validID;

  beforeAll(async () => {
    FavsMock = {
      nameFavorite: `Favorito ${generateString()}`,
      description: "description prueba",
      listFav: "625b845ab6267384037c4205",
    };
    FavsMockNew = {
      nameFavorite: `Favorito ${generateString()}`,
      description: "description prueba",
      listFav: "625b845ab6267384037c4205",
    };
    invalidID = "12323asda";
    validID = "1234567890abcdefghij1234";
    // Login to get token
    const instance = axios.create({
      // Nota: tuve problemas al correr en local 8082, asi que en 5000 me funciono
      baseURL: "http://localhost:5000",
      timeout: 1000,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });

    let data = {
      email: "jorge150896@hotmail.com",
      password: "123456@Jv",
    };

    try {
      const login = await instance.post("/auth/local/login/", data);
      const { token: tokenUser, usuario: idUserMongo } = login.data;
      token = tokenUser;
      idUser = idUserMongo._id;
    } catch (error) {
      console.log(error.response);
    }
    console.log("token", token);
  });

  // ✅✔️✅
  it("Success create a Favorite ", async () => {
    const result = await axios.post(
      "http://localhost:5000/api/favoritos",
      FavsMock,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    idFav = result.data._id;

    expect(result.data).toBeDefined();
    expect(result.status).toEqual(201);
    expect(result.data.nameFavorite).toEqual(
      FavsMock.nameFavorite.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo creo
    expect(result.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(result.data._id.length).toBe(24);
  });

  // ❌🔴❌
  it("Error create a Favorite Without Name", async () => {
    try {
      await axios.post(
        "http://localhost:5000/api/favoritos",
        { nameListFav: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ❌🔴❌
  it("Error create a Favorite With name already registered", async () => {
    try {
      await axios.post("http://localhost:5000/api/favoritos", FavsMock, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.msg).toContain(
        `El elemento favorito ${FavsMock.nameFavorite.toUpperCase()}, ya existe`
      );
    }
  });

  // ✅✔️✅

  it("Success get all Favorites", async () => {
    const result = await axios.get("http://localhost:5000/api/favoritos");
    expect(result.status).toEqual(200);
    expect(Array.isArray(result.data.favoritos)).toEqual(true);
    expect(result.data.favoritos.length).toBeGreaterThan(0);
  });

  // ✅✔️✅

  it("Success get one Favorite", async () => {
    const favsGet = await axios.get(
      `http://localhost:5000/api/favoritos/${idFav}`
    );

    expect(favsGet.status).toEqual(200);
    expect(favsGet.data.favorito.nameFavorite).toEqual(
      FavsMock.nameFavorite.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo creo
    expect(favsGet.data.favorito.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(favsGet.data.favorito._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error get one Favorite with ID invalid", async () => {
    try {
      await axios.get(`http://localhost:5000/api/favoritos/${invalidID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error get one Favorite with ID Valid Non-existent ", async () => {
    try {
      await axios.get(`http://localhost:5000/api/favoritos/${validID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      expect(data.errors[0].value.length).toBe(24);
    }
  });

  // ✅✔️✅
  it("Success Update one Favorite", async () => {
    const favUpdate = await axios.put(
      `http://localhost:5000/api/favoritos/${idFav}`,
      FavsMockNew,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    expect(favUpdate.status).toEqual(200);
    expect(favUpdate.data.nameFavorite).toEqual(
      FavsMockNew.nameFavorite.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo actualizo
    expect(favUpdate.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(favUpdate.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Update one Favorite Without Name", async () => {
    try {
      await axios.put(
        `http://localhost:5000/api/favoritos/${idFav}`,
        { nameFavorite: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ✅✔️✅

  it("Success Delete one Favorite", async () => {
    // search id to test
    const favDelete = await axios.delete(
      `http://localhost:5000/api/favoritos/${idFav}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    expect(favDelete.status).toEqual(200);
    expect(favDelete.data.nameFavorite).toEqual(
      FavsMockNew.nameFavorite.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo elimino
    expect(favDelete.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(favDelete.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Delete one Favorite with ID invalid", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/favoritos/${invalidID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error Delete one Favorite with ID Valid Non-existent ", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/favoritos/${validID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).toBe(24);
    }
  });
});
