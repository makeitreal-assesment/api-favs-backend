const { response } = require("express");
const { Usuario, ListFav, Favorito } = require("../models");

const coleccionesPermitidas = ["usuarios", "listfavs", "favoritos", "roles"];

const buscarUsuarios = async (termino = "", res = response) => {
  const usuarios = await Usuario.searchUser(termino, res);
  res.json({
    results: usuarios,
  });
};

const buscarListFavs = async (termino = "", res = response) => {
  const listfavs = await ListFav.searchListFav(termino, res);
  res.json({
    results: listfavs,
  });
};

const buscarFavoritos = async (termino = "", res = response) => {
  const favoritos = await Favorito.searchFavorito(termino, res);
  res.json({
    results: favoritos,
  });
};

const buscar = (req, res = response) => {
  const { coleccion, termino } = req.params;

  if (!coleccionesPermitidas.includes(coleccion)) {
    return res.status(400).json({
      msg: `Las colecciones permitidas son: ${coleccionesPermitidas}`,
    });
  }

  switch (coleccion) {
    case "usuarios":
      buscarUsuarios(termino, res);
      break;
    case "listfavs":
      buscarListFavs(termino, res);
      break;
    case "favoritos":
      buscarFavoritos(termino, res);
      break;
    default:
      res.status(500).json({
        msg: "Se le olvido hacer esta búsquda",
      });
  }
};

module.exports = {
  buscar,
};
