const { response } = require("express");
const { Favorito } = require("../models");

const obtenerFavoritos = async (req, res = response) => {
  const [total, favoritos] = await Favorito.findAll(req);
  res.status(200).json({
    total,
    favoritos,
  });
};

const obtenerFavorito = async (req, res = response) => {
  const favorito = await Favorito.findOneSingle(req);
  res.status(200).json({ favorito: favorito });
};

const crearFavorito = async (req, res = response) => {
  try {
    const favoritoComplete = await Favorito.create(req, res);
    // res.status(201).json(favorito);
    res.status(201).json(favoritoComplete);
  } catch {
    console.log("Se lanzo Error de Status 400");
  }
};

const actualizarFavorito = async (req, res = response) => {
  try {
    const favorito = await Favorito.modify(req, res);
    res.status(200).json(favorito);
  } catch {
    console.log("Se lanzo Error de Status 400");
  }
};

const borrarFavorito = async (req, res = response) => {
  const favoritoBorrado = await Favorito.remove(req);
  res.status(200).json(favoritoBorrado);
};

module.exports = {
  crearFavorito,
  obtenerFavoritos,
  obtenerFavorito,
  actualizarFavorito,
  borrarFavorito,
};
