const mongoose = require("mongoose");
const {
  obtenerListFavs,
  obtenerListFav,
  borrarListFav,
} = require("../listfavs");
const Listfav = require("../../models/listfav.js");
const req = require("express/lib/request");

const mockRequest = (body, params) => {
  const req = {};
  req.body = { ...body };
  req.params = { ...params };
  req.query = { limite: 5, desde: 0 };
  return req;
};

const mockResponse = () => {
  const res = {};

  function response(data) {
    res.data = data;
  }

  res.send = jest.fn();
  res.json = jest.fn().mockImplementation(response);
  res.next = jest.fn();
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("Listfav controller tests", () => {
  beforeAll(async () => {
    const dbConnection =
      "mongodb+srv://user_apifavs_back:MILAGRINOS1521APIFAVS@apifavs.eyknm.mongodb.net/test_db";
    await mongoose.connect(dbConnection);
  });

  // ✅✔️✅

  it("should get all Listfavs", async () => {
    const req = mockRequest();
    const res = mockResponse();
    await obtenerListFavs(req, res);

    expect(res.status).toHaveBeenCalled(); // que sea llamado
    expect(res.status).toHaveBeenCalledWith(200); // que sea llamado con status 200
    expect(res.json).toHaveBeenCalled(); // que sea llamado
    expect(res.json).toHaveBeenCalledWith(res.data); // que sea llamado con data
  });

  // ✅✔️✅

  it("should get one Listfav", async () => {
    const listfav = await Listfav.findOne();
    const spy = jest.spyOn(Listfav, "findById"); //mode lo y funcion que vamos a espiar
    const req = mockRequest({}, { id: listfav._id });
    const res = mockResponse();
    await obtenerListFav(req, res);

    expect(spy).toHaveBeenCalled(); // que sea llamado
    expect(res.status).toHaveBeenCalled(); // que sea llamado
    expect(res.status).toHaveBeenCalledWith(200); // que sea llamado con status 200
    expect(res.json).toHaveBeenCalled(); // que sea llamado
    expect(res.json).toHaveBeenCalledWith(res.data); // que sea llamado con data
  });
});
