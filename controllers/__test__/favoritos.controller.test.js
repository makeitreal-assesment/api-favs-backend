const mongoose = require("mongoose");
const {
  obtenerFavoritos,
  obtenerFavorito,
  borrarFavorito,
} = require("../favoritos");
const Favorito = require("../../models/favorito");

const mockRequest = (body, params) => {
  const req = {};
  req.body = { ...body };
  req.params = { ...params };
  req.query = { limite: 5, desde: 0 };
  return req;
};

const mockResponse = () => {
  const res = {};

  function response(data) {
    res.data = data;
  }

  res.send = jest.fn();
  res.json = jest.fn().mockImplementation(response);
  res.next = jest.fn();
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("Favorito controller tests", () => {
  beforeAll(async () => {
    const dbConnection =
      "mongodb+srv://user_apifavs_back:MILAGRINOS1521APIFAVS@apifavs.eyknm.mongodb.net/test_db";
    await mongoose.connect(dbConnection);
  });

  // ✅✔️✅

  it("should get all Favoritos", async () => {
    const req = mockRequest();
    const res = mockResponse();
    await obtenerFavoritos(req, res);

    expect(res.status).toHaveBeenCalled(); // que sea llamado
    expect(res.status).toHaveBeenCalledWith(200); // que sea llamado con status 200
    expect(res.json).toHaveBeenCalled(); // que sea llamado
    expect(res.json).toHaveBeenCalledWith(res.data); // que sea llamado con data
  });

  // ✅✔️✅

  it("should get one Favorito", async () => {
    const favorito = await Favorito.findOne();
    const spy = jest.spyOn(Favorito, "findById"); //mode lo y funcion que vamos a espiar
    const req = mockRequest({}, { id: favorito._id });
    const res = mockResponse();
    await obtenerFavorito(req, res);

    expect(spy).toHaveBeenCalled(); // que sea llamado
    expect(res.status).toHaveBeenCalled(); // que sea llamado
    expect(res.status).toHaveBeenCalledWith(200); // que sea llamado con status 200
    expect(res.json).toHaveBeenCalled(); // que sea llamado
    expect(res.json).toHaveBeenCalledWith(res.data); // que sea llamado con data
  });
});
