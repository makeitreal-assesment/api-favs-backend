const { response, request } = require("express");
const { Usuario } = require("../models");

const usuariosGetAll = async (req = request, res = response) => {
  const [total, usuarios] = await Usuario.findAll(req);
  res.status(200).json({
    total,
    usuarios,
  });
};

const usuariosGet = async (req, res = response) => {
  const usuarioComplete = await Usuario.findOneSingle(req);
  res.status(200).json({ usuario: usuarioComplete });
};

const usuariosPost = async (req, res = response) => {
  const usuario = await Usuario.create(req);
  res.status(201).json({
    status: 1,
    usuario,
  });
};

const usuariosPut = async (req, res = response) => {
  const usuario = await Usuario.modify(req);
  res.status(200).json({ status: 1, usuario });
  //   res.json({ usuario,}); // si lo encierro en llaves, sale usuario:{} en la respuesta
};

const usuariosDelete = async (req, res = response) => {
  const usuario = await Usuario.remove(req);
  res.status(200).json(usuario);
};

module.exports = {
  usuariosGetAll,
  usuariosGet,
  usuariosPost,
  usuariosPut,
  usuariosDelete,
};
