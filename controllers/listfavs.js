const { response } = require("express");
const { ListFav } = require("../models");

const obtenerListFavs = async (req, res = response) => {
  const [total, listfavs] = await ListFav.findAll(req);
  res.status(200).json({
    total,
    listfavs,
  });
};

const obtenerListFav = async (req, res = response) => {
  const listfavComplete = await ListFav.findOneSingle(req);
  // res.json({ listfav: listfav });
  res.status(200).json({ listfav: listfavComplete });
};

const crearListFav = async (req, res = response) => {
  try {
    const listfavComplete = await ListFav.create(req, res);
    res.status(201).json(listfavComplete); // SI SE CREA ALGo se manda el 201
  } catch {
    console.log("Se lanzo Error de Status 400");
  }
  // res.status(201).json(listfav); // SI SE CREA ALGo se manda el 201
};

const actualizarListFav = async (req, res = response) => {
  try {
    const listfav = await ListFav.modify(req, res);
    res.status(200).json(listfav);
  } catch {
    console.log("Se lanzo Error de Status 400");
  }
};

const borrarListFav = async (req, res = response) => {
  const listfavBorrada = await ListFav.remove(req);
  res.status(200).json(listfavBorrada);
};

module.exports = {
  crearListFav,
  obtenerListFavs,
  obtenerListFav,
  actualizarListFav,
  borrarListFav,
};
