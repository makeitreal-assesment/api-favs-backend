const { response } = require("express");
const Usuario = require("../models/usuario");

const login = async (req, res = response) => {
  const [usuario, validPassword, token] = await Usuario.userAuth(req);

  if (!usuario) {
    return res.status(400).json({
      msg: "Usuario / Password no son correctos - email",
    });
  }
  if (!usuario.state) {
    return res.status(400).json({
      msg: "Usuario / Password no son correctos - estado: false",
    });
  }
  if (!validPassword) {
    return res.status(400).json({
      msg: "Usuario / Password no son correctos - password",
    });
  }

  res.json({
    usuario,
    token,
  });
};

module.exports = {
  login,
};
