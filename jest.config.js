const config = {
  verbose: true,
  testTimeout: 20000,
};

module.exports = config;
