const Role = require("../models/role");
const { Usuario, ListFav, Favorito } = require("../models");

const esRoleValido = async (rol = "") => {
  const existeRol = await Role.findOne({ rol });
  if (!existeRol) {
    throw new Error(`El rol ${rol} no está registrado en la BD`);
  }
};

const emailExiste = async (email = "") => {
  // Verificar si el email existe
  const existeEmail = await Usuario.findOne({ email });
  if (existeEmail) {
    throw new Error(`El email: ${email}, ya está registrado`);
  }
};

const existeUsuarioPorId = async (id) => {
  const existeUsuario = await Usuario.findById(id);
  if (!existeUsuario) {
    throw new Error(`El id no existe ${id}`);
  }
};
/**
 * Lista Favoritos
 */
const existeListFavPorId = async (id) => {
  const existeListFav = await ListFav.findById(id);
  if (!existeListFav) {
    throw new Error(`El id no existe ${id}`);
  }
};

/**
 * Favoritos
 */
const existeFavoritoPorId = async (id) => {
  const existeFavorito = await Favorito.findById(id);
  if (!existeFavorito) {
    throw new Error(`El id no existe ${id}`);
  }
};
/**
 * Validar que al crear un Favorito , petenece a la lista del usuario logueado
 */

/**
 * Validar colecciones permitidas
 */
const coleccionesPermitidas = (coleccion = "", colecciones = []) => {
  const incluida = colecciones.includes(coleccion);
  if (!incluida) {
    throw new Error(
      `La colección ${coleccion} no es permitida, ${colecciones}`
    );
  }
  return true;
};

module.exports = {
  esRoleValido,
  emailExiste,
  existeUsuarioPorId,
  existeListFavPorId,
  existeFavoritoPorId,
  coleccionesPermitidas,
};
