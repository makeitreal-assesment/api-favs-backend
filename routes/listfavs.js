const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarJWT,
  validarCampos,
  esAdminRole,
  tieneRole,
} = require("../middlewares");

const {
  crearListFav,
  obtenerListFavs,
  obtenerListFav,
  actualizarListFav,
  borrarListFav,
} = require("../controllers/listfavs");

const { existeListFavPorId } = require("../helpers/db-validators");

const router = Router();

router.get("/", obtenerListFavs);

// Obtener una lista de Favoritos por id - publico
router.get(
  "/:id",
  [
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeListFavPorId),
    validarCampos,
  ],
  obtenerListFav
);

// Crear lista de Favoritos - privado - cualquier persona con un token válido
router.post(
  "/",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN modificar una lista de Favoritos
    check("nameListFav", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearListFav
);

// Actualizar - privado - cualquiera con token válido
router.put(
  "/:id",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN modificar una lista de Favoritos
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeListFavPorId),
    check("nameListFav", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  actualizarListFav
);

// Borrar una lista de Favoritos - Admin
router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN modificar una lista de Favoritos
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeListFavPorId),
    validarCampos,
  ],
  borrarListFav
);

module.exports = router;
