const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarJWT,
  validarCampos,
  esAdminRole,
  tieneRole,
} = require("../middlewares");

const {
  crearFavorito,
  obtenerFavoritos,
  obtenerFavorito,
  actualizarFavorito,
  borrarFavorito,
} = require("../controllers/favoritos");

const {
  existeListFavPorId,
  existeFavoritoPorId,
} = require("../helpers/db-validators");

const router = Router();

//  Obtener todas las favoritos - publico
router.get("/", obtenerFavoritos);

// Obtener un favoritos por id - publico
router.get(
  "/:id",
  [
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeFavoritoPorId),
    validarCampos,
  ],
  obtenerFavorito
);

// Crear Lista favoritos - privado - cualquier persona con un token válido
router.post(
  "/",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO

    check("nameFavorite", "El nombre es obligatorio").not().isEmpty(),
    check("listFav", "No es un id de Mongo").isMongoId(),
    validarCampos,
    check("listFav").custom(existeListFavPorId),
    validarCampos,
  ],
  crearFavorito
);

// Actualizar - privado - cualquiera con token válido
router.put(
  "/:id",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO

    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeFavoritoPorId),
    check("nameFavorite", "El nombre es obligatorio").not().isEmpty(),

    validarCampos,
  ],
  actualizarFavorito
);

// Borrar una Lista de favoritos - Admin
router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeFavoritoPorId),
    validarCampos,
  ],
  borrarFavorito
);

module.exports = router;
