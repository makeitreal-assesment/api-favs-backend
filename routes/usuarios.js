const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarCampos,
  validarJWT,
  esAdminRole,
  tieneRole,
} = require("../middlewares");

const {
  esRoleValido,
  emailExiste,
  existeUsuarioPorId,
} = require("../helpers/db-validators");

const {
  usuariosGetAll,
  usuariosGet,
  usuariosPut,
  usuariosPost,
  usuariosDelete,
  usuariosPatch,
} = require("../controllers/usuarios");

const router = Router();

router.get("/", usuariosGetAll);
router.get(
  "/:id",
  [
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    validarCampos,
  ],
  usuariosGet
);

router.put(
  "/:id",
  [
    validarJWT,
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    check("rol").custom(esRoleValido),
    validarCampos,

    check("password", "El password debe de ser más de 6 letras").isLength({
      min: 6,
    }),
    check("password", "El password debe contener un caracter especial").matches(
      /[!@#$%^&*(),.?":{}|<>]/
    ),
    check("password", "El password debe contener una letra mayúscula").matches(
      /(?=.*[A-Z])/
    ),
    check("password", "El password debe contener una letra minúscula").matches(
      /(?=.*[a-z])/
    ),
    // .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i");
    validarCampos, //middleware que revisa los errores de las validaciones de los checks, si esto pasa, ejecuta el controlador, sino no ejecuta
  ],
  usuariosPut
);

router.post(
  "/",
  [
    check("names", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
    check("rol").custom(esRoleValido),
    validarCampos,
    check("email", "El email no es válido").isEmail(),
    check("email").custom(emailExiste),
    // check("rol", "No es un rol válido").isIn(["ADMIN_ROLE", "USER_ROLE"]),
    validarCampos,

    check("password", "El password debe de ser más de 6 letras").isLength({
      min: 6,
    }),
    check("password", "El password debe contener un caracter especial").matches(
      /[!@#$%^&*(),.?":{}|<>]/
    ),
    check("password", "El password debe contener una letra mayúscula").matches(
      /(?=.*[A-Z])/
    ),
    check("password", "El password debe contener una letra minúscula").matches(
      /(?=.*[a-z])/
    ),
    // .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i");
    validarCampos, //middleware que revisa los errores de las validaciones de los checks, si esto pasa, ejecuta el controlador, sino no ejecuta
  ],
  usuariosPost
);

router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole, //A FUERZA PIDE QUE SEA ADMINISTRADOR
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    validarCampos,
  ],
  usuariosDelete
);

module.exports = router;
