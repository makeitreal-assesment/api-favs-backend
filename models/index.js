const ListFav = require("./listfav");
const Favorito = require("./favorito");
const Role = require("./role");
const Usuario = require("./usuario");

module.exports = {
  ListFav,
  Favorito,
  Role,
  Usuario,
};
