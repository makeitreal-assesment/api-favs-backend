const { Schema, model } = require("mongoose");
const bcryptjs = require("bcryptjs");
const ListFav = require("./listfav");
const { ObjectId } = require("mongoose").Types;
const { generarJWT } = require("../helpers/generar-jwt");

const UsuarioSchema = Schema({
  names: {
    type: String,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    required: [true, "El correo es obligatorio"],
    unique: true,
  },
  password: {
    type: String,
    required: [true, "La contraseña es obligatoria"],
  },
  rol: {
    type: String,
    required: true,
    emun: ["ADMIN_ROLE", "USER_ROLE"],
  },
  state: {
    type: Boolean,
    default: true,
  },
});

/*
 statics
 */

UsuarioSchema.statics.findAll = async function (req) {
  const { limite = 5, desde = 0 } = req.query;
  const query = { state: true }; // para solo traer usuarios con estado true, activos

  const [total, data] = await Promise.all([
    this.countDocuments(query), // para solo traer usuarios con estado true, activos
    this.find(query).skip(Number(desde)).limit(Number(limite)),
  ]);
  return [total, data];
};

UsuarioSchema.statics.findOneSingle = async function (req) {
  const { id } = req.params;

  const usuario = await this.findById(id);

  const { _id, names, email, rol, state } = usuario;
  const listFavs = await ListFav.find({ usuario: _id.toString() });

  const usuarioComplete = {
    _id,
    names,
    email,
    rol,
    state,
    listFavs,
  };
  return usuarioComplete;
};

UsuarioSchema.statics.create = async function (req) {
  const {
    names,
    email,
    password,
    rol,
    ...resto // con esto ya solo trae todos los demas
  } = req.body;
  const usuario = new this({
    names,
    email,
    password,
    rol,
    ...resto, // con esto ya solo trae todos los demas
  });

  // Encriptar la contraseña
  const salt = bcryptjs.genSaltSync(); //numero de vueltas para la encriptacion)mas vueltas mas complejo, por defecto esta en 10, como bcryptjs.genSaltSync(10)
  usuario.password = bcryptjs.hashSync(password, salt);

  // Guardar en BD
  await usuario.save();

  return usuario;
};

UsuarioSchema.statics.modify = async function (req) {
  const { id } = req.params;
  const { _id, password, email, ...resto } = req.body;
  // extrajimos email, para que no se pueda modificar estos datos
  // el password extrajimos para validar que exista y hacer su cambio

  //TODO validar contra base de datos

  if (password) {
    // el passwrod si llega significa que queremos cambiarlo

    // Encriptar la contraseña
    const salt = bcryptjs.genSaltSync();
    resto.password = bcryptjs.hashSync(password, salt);
  }

  const usuario = await this.findByIdAndUpdate(id, resto, { new: true }); //busca el id y actualiza los datos, ademas se añadio {new:true}, para que retorne los valores actuales al cambio

  return usuario;
};

UsuarioSchema.statics.remove = async function (req) {
  const { id } = req.params;

  // Fisicamente lo borramos
  // const usuario = await Usuario.findByIdAndDelete( id ); //Esto si de verdad queremos quitarlo de la BD

  const usuario = await this.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );

  return usuario;
};

UsuarioSchema.statics.userAuth = async function (req) {
  const { email, password } = req.body;
  const usuario = await this.findOne({ email });
  let validPassword;
  let token;

  if (usuario && usuario.state) {
    validPassword = bcryptjs.compareSync(password, usuario.password);
  }
  if (validPassword) {
    token = await generarJWT(usuario.id);
  }
  return [usuario, validPassword, token];
};

UsuarioSchema.statics.searchUser = async function (termino, res) {
  const esMongoID = ObjectId.isValid(termino); // TRUE

  if (esMongoID) {
    const usuario = await this.findById(termino);
    return res.json({
      results: usuario ? [usuario] : [],
    });
  }

  const regex = new RegExp(termino, "i");
  const usuarios = await this.find({
    $or: [{ names: regex }, { email: regex }],
    $and: [{ state: true }],
  });

  return usuarios;
};

//Debe ser funcion normal y no arrow function
UsuarioSchema.methods.toJSON = function () {
  const { __v, password, ...usuario } = this.toObject();
  return usuario;
};

module.exports = model("Usuario", UsuarioSchema);
