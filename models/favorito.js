const { Schema, model } = require("mongoose");
const { ObjectId } = require("mongoose").Types;

const FavoritoSchema = Schema({
  nameFavorite: {
    type: String,
    required: [true, "El nombre es obligatorio"],
    // unique: true,
  },
  description: {
    type: String,
    default: "",
  },
  link: {
    type: String,
    default: "",
  },
  state: {
    type: Boolean,
    default: true,
    required: true,
  },
  usuario: {
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
  listFav: {
    type: Schema.Types.ObjectId,
    ref: "ListFav",
    required: true,
  },
});

/*
 statics
 */
FavoritoSchema.statics.findAll = async function (req) {
  const { limite = 5, desde = 0 } = req.query;
  const query = { state: true };

  const [total, data] = await Promise.all([
    this.countDocuments(query),
    this.find(query)
      .populate("usuario", "names")
      .populate("listFav", "nameListFav")
      .skip(Number(desde))
      .limit(Number(limite)),
  ]);

  return [total, data];
};

FavoritoSchema.statics.findOneSingle = async function (req) {
  const { id } = req.params;
  const favorito = await this.findById(id)
    .populate("usuario", "names")
    .populate("listFav", "nameListFav");

  return favorito;
};

FavoritoSchema.statics.create = async function (req, res) {
  const { state, usuario, ...body } = req.body;

  const nameFavorite = body.nameFavorite.toUpperCase(); //lo agregue a q sea en mayuscula para reconcoer el favorito que queria agregar, y me salga el error de que existe,ya que el guardado anterior lo hacia con mayuscula

  //Lo uso para filtrar si esa favorito existe, solo si el usuario logueado lo tiene creado
  const favoritoDB = await this.findOne({
    $and: [{ nameFavorite: nameFavorite, usuario: req.usuario }],
  });
  // Se filtra cuando validaba que exista ese favorito sin importar quien lo creo
  // const favoritoDB = await Favorito.findOne({ nameFavorite }); //lo agregue a q sea en mayuscula para reconocer el favorito que queria agregar, y me salga el error de que existe,ya que el guardado anterior lo hacia con mayuscula

  if (favoritoDB) {
    return res.status(400).json({
      msg: `El elemento favorito ${favoritoDB.nameFavorite}, ya existe`,
    });
  }

  // Generar la data a guardar
  const data = {
    ...body,
    nameFavorite: body.nameFavorite.toUpperCase(),
    usuario: req.usuario._id,
  };

  const favorito = new this(data);

  // Guardar DB
  await favorito.save();

  const datalink = {
    link: `${process.env.HOST_URL}/api/favoritos/${favorito._id}`,
  };

  const favoritoComplete = await this.findByIdAndUpdate(
    favorito._id,
    datalink,
    { new: true }
  )
    .populate("usuario", "names")
    .populate("listFav", "nameListFav");

  return favoritoComplete;
};

FavoritoSchema.statics.modify = async function (req, res) {
  const { id } = req.params;
  const { state, usuario, ...data } = req.body;

  if (data.nameFavorite) {
    data.nameFavorite = data.nameFavorite.toUpperCase();
  }

  data.usuario = req.usuario._id;

  //Lo uso para filtrar si esa favorito existe, solo si el usuario logueado lo tiene creado
  const favoritoDB = await this.findOne({
    $and: [{ nameFavorite: data.nameFavorite, usuario: req.usuario }],
  });
  // Se filtra cuando validaba que exista ese favorito sin importar quien lo creo
  // const favoritoDB = await Favorito.findOne({ nameFavorite }); //lo agregue a q sea en mayuscula para reconocer el favorito que queria agregar, y me salga el error de que existe,ya que el guardado anterior lo hacia con mayuscula

  if (favoritoDB) {
    return res.status(400).json({
      msg: `El elemento favorito ${favoritoDB.nameFavorite}, ya existe`,
    });
  }

  const favorito = await this.findByIdAndUpdate(id, data, { new: true })
    .populate("usuario", "names")
    .populate("listFav", "nameListFav");

  return favorito;
};

FavoritoSchema.statics.remove = async function (req) {
  const { id } = req.params;
  const favoritoBorrado = await this.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  )
    .populate("usuario", "names")
    .populate("listFav", "nameListFav");

  return favoritoBorrado;
};

FavoritoSchema.statics.searchFavorito = async function (termino, res) {
  const esMongoID = ObjectId.isValid(termino); // TRUE

  if (esMongoID) {
    const favorito = await this.findById(termino)
      .populate("usuario", "names")
      .populate("listFav", "nameListFav");
    return res.json({
      results: favorito ? [favorito] : [],
    });
  }

  const regex = new RegExp(termino, "i");
  const favoritos = await this.find({
    nameFavorite: regex,
    state: true,
  })
    .populate("usuario", "names")
    .populate("listFav", "nameListFav");

  return favoritos;
};

FavoritoSchema.methods.toJSON = function () {
  const { __v, state, ...data } = this.toObject();
  return data;
};

module.exports = model("Favorito", FavoritoSchema);
