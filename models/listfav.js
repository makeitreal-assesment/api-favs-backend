const { Schema, model } = require("mongoose");
const { ObjectId } = require("mongoose").Types;

const Favorito = require("./favorito");

const ListFavSchema = Schema({
  nameListFav: {
    type: String,
    required: [true, "El nombre es obligatorio"],
    // unique: true,
  },
  state: {
    type: Boolean,
    default: true,
    required: true,
  },
  usuario: {
    //para saber que usuario creo la ListFav
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
});

/*
 statics
 */

ListFavSchema.statics.findAll = async function (req) {
  const { limite = 5, desde = 0 } = req.query;
  const query = { state: true };

  const [total, data] = await Promise.all([
    this.countDocuments(query),
    this.find(query)
      .populate("usuario", "names")
      .skip(Number(desde))
      .limit(Number(limite)),
  ]);

  return [total, data];
};

ListFavSchema.statics.findOneSingle = async function (req) {
  const { id } = req.params;
  const listfav = await this.findById(id).populate("usuario", "names");

  const { _id, nameListFav, usuario } = listfav;
  const favoritos = await Favorito.find({ listFav: _id.toString() });

  const listfavComplete = {
    _id,
    nameListFav,
    usuario,
    favoritos,
  };

  return listfavComplete;
};

ListFavSchema.statics.create = async function (req, res) {
  const nameListFav = req.body.nameListFav.toUpperCase();

  //Lo uso para filtrar si esa Lista existe, solo si el usuario logueado lo tiene creado
  const listfavDB = await this.findOne({
    $and: [{ nameListFav: nameListFav, usuario: req.usuario }],
  });
  // Se filtra cuando validaba que exista esa lista sin importar quien lo creo
  // const listfavDB = await ListFav.findOne({
  //   nameListFav,
  // });

  if (listfavDB) {
    return res.status(400).json({
      msg: `La Lista de Favoritos ${listfavDB.nameListFav}, ya existe`,
    });
  }

  // Generar la data a guardar
  const data = {
    nameListFav,
    usuario: req.usuario._id,
  };

  const listfav = new this(data);

  // Guardar DB
  await listfav.save();
  const listfavComplete = await this.findById(listfav._id).populate(
    "usuario",
    "names"
  );

  return listfavComplete;
};

ListFavSchema.statics.modify = async function (req, res) {
  const { id } = req.params;
  const { state, usuario, ...data } = req.body;
  data.nameListFav = data.nameListFav.toUpperCase();
  data.usuario = req.usuario._id;

  //Lo uso para filtrar si esa Lista existe, solo si el usuario logueado lo tiene creado
  const listfavDB = await this.findOne({
    $and: [{ nameListFav: data.nameListFav, usuario: req.usuario }],
  });

  if (listfavDB) {
    return res.status(400).json({
      msg: `La Lista de Favoritos ${listfavDB.nameListFav}, ya existe`,
    });
  }

  const listfav = await this.findByIdAndUpdate(id, data, {
    new: true,
  }).populate("usuario", "names");

  return listfav;
};

ListFavSchema.statics.remove = async function (req) {
  const { id } = req.params;
  const listfavBorrada = await this.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  ).populate("usuario", "names");

  return listfavBorrada;
};

ListFavSchema.statics.searchListFav = async function (termino, res) {
  const esMongoID = ObjectId.isValid(termino); // TRUE

  if (esMongoID) {
    const listfav = await this.findById(termino).populate("usuario", "names");
    return res.json({
      results: listfav ? [listfav] : [],
    });
  }

  const regex = new RegExp(termino, "i"); //hacemos que la expresion buscada sea insensible a mayusculas y minusculas
  const listfavs = await this.find({
    nameListFav: regex,
    state: true,
  }).populate("usuario", "names");

  return listfavs;
};

ListFavSchema.methods.toJSON = function () {
  const { __v, state, ...data } = this.toObject();
  return data;
};

module.exports = model("ListFav", ListFavSchema);
