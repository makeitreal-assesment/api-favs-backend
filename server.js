const express = require("express");
const cors = require("cors");
const { dbConnection } = require("./database/config");

const dotenv = require("dotenv");
const path = require("path");
const url = require("url");

// config environments
console.log("Dirname ", path.join(__dirname, `${process.env.NODE_ENV}.env`));
dotenv.config({
  path: path.resolve(__dirname, `${process.env.NODE_ENV}.env`),
});

console.log(process.env.MONGODB_CNN);

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    // this.usuariosPath = "/api/usuarios";
    // this.authPath = "/auth/local";
    this.paths = {
      auth: "/auth/local",
      buscar: "/api/buscar",
      favoritos: "/api/favoritos",
      listfavs: "/api/listfavs",
      usuarios: "/api/usuarios",
    };

    // Conectar a base de datos
    this.conectarDB();

    // Middlewares
    this.middlewares();

    // Rutas de mi aplicación
    this.routes();
  }

  async conectarDB() {
    await dbConnection();
  }

  middlewares() {
    // CORS
    this.app.use(cors());

    // Lectura y parseo del body
    this.app.use(express.json());

    // Directorio Público
    this.app.use(express.static("public"));
  }

  routes() {
    this.app.use(this.paths.auth, require("./routes/auth"));
    this.app.use(this.paths.buscar, require("./routes/buscar"));
    this.app.use(this.paths.favoritos, require("./routes/favoritos"));
    this.app.use(this.paths.listfavs, require("./routes/listfavs"));
    this.app.use(this.paths.usuarios, require("./routes/usuarios"));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en puerto", this.port);
    });
  }
}

module.exports = Server;
