# API FAVS Backend

### [API Despliegue](https://apifavs.herokuapp.com/ "API Despliegue")

### [API Documentation](https://documenter.getpostman.com/view/17190637/Uyr5oeVR "API Documentation")

Favs is a new company that aims to provide a better way to organize your favorite things: music, clothes, courses, etc., all in one place.

## Installation

Requires [Node.js](https://nodejs.org/) v16+ to run.

Install the dependencies and devDependencies and start the server.

```
npm install
```

For production environments...

```
PORT=
MONGODB_CNN=
SECRETORPRIVATEKEY
HOST_URL=
```

## Run

Development:

```sh
npm run dev
```

Production:

```sh
npm start
```

Test:

```sh
npm run server-test
npm run test
```

## Features

- feature/configInitial
- feature/userCrud-v2
- feature/jwt-v3
- feature/CRUDFavs-v4
- feature/optimizaciones-v5
- feature/testing-v6

## License

MIT

## :+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Youtube: (https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)

<table>
    <td align="center" >
      <a href="https://jorge-vicuna.gitlab.io/jorge-vicuna/">
        <img src="https://jorge-vicuna.gitlab.io/jorge-vicuna/static/media/avatar.272f0e79.jpg" width="100px;" alt=""/>
        <br />
        <sub><b>Jorge Vicuña Valle</b></sub>
      </a>
            <br />
      <span>♌🍗🎸🏀</span>
    </td>
</Table>
